# Generated by Django 3.0.3 on 2020-04-11 18:34

import datetime
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logbook', '0029_auto_20200411_1827'),
    ]

    operations = [
        migrations.AddField(
            model_name='rechnung',
            name='kennz',
            field=models.CharField(default='AAA-BB_1', help_text='Wählen Sie das Fahrzeug aus.', max_length=32, validators=[django.core.validators.RegexValidator('^[A-Z]{1,3}-[A-Z]{1,2}_[1-9][0-9]{0,2}E?$', 'Kennzeichen bitte nach folgendem Schema angeben: ABC-DE_123E (E ist optional)')], verbose_name='Kennzeichen'),
        ),
        migrations.AlterField(
            model_name='fahrer',
            name='work_id',
            field=models.CharField(help_text='Die Mitarbeiternummer in Ihrer Organisation', max_length=32),
        ),
        migrations.AlterField(
            model_name='mangel',
            name='dat',
            field=models.DateField(default=datetime.datetime(2020, 4, 11, 18, 34, 25, 23983), verbose_name='Datum'),
        ),
        migrations.AlterField(
            model_name='mangel',
            name='kennz',
            field=models.CharField(default='AAA-BB_1', help_text='Wählen Sie das Fahrzeug aus.', max_length=32, validators=[django.core.validators.RegexValidator('^[A-Z]{1,3}-[A-Z]{1,2}_[1-9][0-9]{0,2}E?$', 'Kennzeichen bitte nach folgendem Schema angeben: ABC-DE_123E (E ist optional)')], verbose_name='Kennzeichen'),
        ),
    ]
