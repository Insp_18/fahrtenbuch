# Generated by Django 3.0.3 on 2020-04-14 20:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('logbook', '0037_auto_20200414_1831'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mangel',
            name='fk',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='logbook.Fahrzeug'),
        ),
    ]
