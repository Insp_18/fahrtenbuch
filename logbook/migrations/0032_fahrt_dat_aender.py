# Generated by Django 3.0.3 on 2020-04-13 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logbook', '0031_auto_20200412_1150'),
    ]

    operations = [
        migrations.AddField(
            model_name='fahrt',
            name='dat_aender',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
