# Generated by Django 3.0.3 on 2020-04-09 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logbook', '0015_auto_20200409_1647'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fahrt',
            name='dat',
            field=models.DateTimeField(),
        ),
    ]
