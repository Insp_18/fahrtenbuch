# Generated by Django 3.0.3 on 2020-04-09 15:30

from django.db import migrations, models
import logbook.models


class Migration(migrations.Migration):

    dependencies = [
        ('logbook', '0016_auto_20200409_1649'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fahrt',
            name='dat',
            field=models.DateTimeField(validators=[logbook.models.validate_date]),
        ),
    ]
