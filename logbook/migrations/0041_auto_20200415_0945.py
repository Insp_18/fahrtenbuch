# Generated by Django 3.0.3 on 2020-04-15 09:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logbook', '0040_auto_20200414_2141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mangel',
            name='behoben',
            field=models.NullBooleanField(help_text='Markieren Sie hier, falls der Mangel behoben wurde'),
        ),
    ]
