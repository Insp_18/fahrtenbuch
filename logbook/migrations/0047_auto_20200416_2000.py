# Generated by Django 3.0.3 on 2020-04-16 20:00

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('logbook', '0046_auto_20200415_1613'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fahrer',
            name='email',
            field=models.EmailField(max_length=128, verbose_name='E-Mail'),
        ),
        migrations.AlterField(
            model_name='fahrer',
            name='fk',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='logbook.Fahrzeug', verbose_name='Fahrzeug'),
        ),
        migrations.AlterField(
            model_name='fahrer',
            name='name',
            field=models.CharField(max_length=256, unique=True, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='fahrer',
            name='tel',
            field=models.CharField(max_length=128, verbose_name='Tel.'),
        ),
        migrations.AlterField(
            model_name='fahrer',
            name='work_id',
            field=models.CharField(help_text='Die Mitarbeiternummer in Ihrer Organisation', max_length=32, verbose_name='Mitarbeiternummer'),
        ),
        migrations.AlterField(
            model_name='fahrzeug',
            name='fin',
            field=models.CharField(max_length=64, unique=True, verbose_name='FIN'),
        ),
        migrations.AlterField(
            model_name='fahrzeug',
            name='kennz',
            field=models.CharField(max_length=32, unique=True, validators=[django.core.validators.RegexValidator('^[A-Z]{1,3}-[A-Z]{1,2}_[1-9][0-9]{0,2}E?$', 'Kennzeichen bitte nach folgendem Schema angeben: ABC-DE_123E (E ist optional)')], verbose_name='Kennzeichen'),
        ),
        migrations.AlterField(
            model_name='fahrzeug',
            name='km_stand',
            field=models.FloatField(default=0, max_length=64, verbose_name='KM-Stand'),
        ),
        migrations.AlterField(
            model_name='fahrzeug',
            name='kost_Stelle',
            field=models.CharField(help_text='Identifikationsnummer Ihrer Buchhaltung', max_length=64, verbose_name='Kostenstelle'),
        ),
        migrations.AlterField(
            model_name='fahrzeug',
            name='tuev',
            field=models.DateTimeField(verbose_name='TÜV-Termin'),
        ),
        migrations.AlterField(
            model_name='fahrzeug',
            name='zuldat',
            field=models.DateTimeField(verbose_name='Zulassung'),
        ),
        migrations.AlterField(
            model_name='rechnung',
            name='betrag',
            field=models.CharField(default='', max_length=16, verbose_name='Betrag'),
        ),
    ]
