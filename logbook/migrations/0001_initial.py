# Generated by Django 3.0.3 on 2020-03-05 17:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Adresse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('street', models.CharField(max_length=128)),
                ('nr', models.CharField(max_length=32)),
                ('plz', models.CharField(max_length=32)),
                ('city', models.CharField(max_length=64)),
                ('ctry', models.CharField(max_length=64)),
                ('slug', models.SlugField()),
            ],
        ),
        migrations.CreateModel(
            name='Fahrer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('work_id', models.CharField(max_length=32)),
                ('email', models.EmailField(max_length=128)),
                ('tel', models.CharField(max_length=128)),
                ('password', models.CharField(default='12345', max_length=128)),
                ('slug', models.SlugField()),
            ],
        ),
        migrations.CreateModel(
            name='Fuhrparkleiter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('work_id', models.CharField(max_length=32)),
                ('email', models.EmailField(max_length=254)),
                ('tel', models.CharField(max_length=128)),
                ('password', models.CharField(default='12345', max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='Mangel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dat', models.DateField()),
                ('slug', models.SlugField()),
            ],
        ),
        migrations.CreateModel(
            name='Rechnung',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rech_num', models.CharField(max_length=64)),
                ('slug', models.SlugField()),
            ],
        ),
        migrations.CreateModel(
            name='Fahrzeug',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kennz', models.CharField(max_length=32)),
                ('fin', models.CharField(max_length=64)),
                ('zuldat', models.DateField()),
                ('tuev', models.DateField()),
                ('kost_Stelle', models.CharField(max_length=64)),
                ('slug', models.SlugField()),
                ('fahrer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='logbook.Fahrer')),
                ('mang', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='logbook.Mangel')),
                ('rech', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='logbook.Rechnung')),
            ],
        ),
        migrations.CreateModel(
            name='Fahrt',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dat', models.DateField()),
                ('time', models.TimeField()),
                ('start', models.CharField(max_length=64)),
                ('zwi_ziel', models.CharField(max_length=64)),
                ('ziel', models.CharField(max_length=64)),
                ('km_start', models.IntegerField()),
                ('km_end', models.IntegerField()),
                ('unvoll', models.BooleanField(blank=True)),
                ('beschr', models.TextField(blank=True, null=True)),
                ('slug', models.SlugField()),
                ('fahrz', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='logbook.Fahrzeug')),
            ],
        ),
    ]
