from django.contrib import admin

from .models import Fahrer, Fahrzeug, Fuhrparkleiter, Mangel, Rechnung, Fahrt

admin.site.register(Fahrer)
admin.site.register(Fahrt)
admin.site.register(Fahrzeug)
admin.site.register(Fuhrparkleiter)
admin.site.register(Mangel)
admin.site.register(Rechnung)
