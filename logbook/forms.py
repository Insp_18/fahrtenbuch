# Modul zum Erstellen von Formen, die automatisch Inhalte als HTML-Form rendern
from django.forms import ModelForm
# Modul zum detaillierteren Konfigurieren der Formen
from django import forms
from .models import *



class FahrerForm(ModelForm):
    class Meta:
        model = Fahrer
        # Felder bestimmen, die mit der Form bearbeitet werden können
        fields = ['name', 'work_id', 'email', 'tel']
        # Felder explizit ausschließen, damit sie nicht angezeigt werden
        exclude = ['password']
        labels = {'name': 'Name', 'work_id': 'Mitarbeiternummer', 'email': 'E-Mail-Adresse', 'tel': 'Telefonnummer'}


class FahrtForm(ModelForm):
    class Meta:
        model = Fahrt
        # das Feld "fk" als HiddenInput rendern
        #widgets = {'fk': forms.HiddenInput()}
        fields = ['dat', 'fk', 'start', 'zwi_ziel', 'ziel', 'km_start', 'km_end', 'beschr']
        # Felder mit sprechenden Labels rendern
        labels = {
            'dat': 'Datum',
            'start': 'Startadresse',
            'zwi_ziel': 'Zwischenziel',
            'ziel': 'Zieladresse' ,
            'km_start': 'km-Stand Start',
            'km_end': 'km-Stand Ende',
            'beschr': 'Anmerkungen'}


class FahrzeugForm(ModelForm):
    class Meta:
        model = Fahrzeug
        fields = ['kennz', 'fin', 'zuldat', 'tuev', 'kost_Stelle', 'herst', 'km_stand']
        labels = {'kennz': 'Kennzeichen', 'fin': 'Fahrzeugidentifikationsnummer (FIN)', 'zuldat': 'Erstzulassungsdatum', 'tuev': 'TÜV/AU-Termin', 'kost_Stelle': 'Kostenstelle Buchhaltung', 'km_stand': 'KM-Stand'}


class RechnungForm(ModelForm):
    class Meta:
        model = Rechnung
        #widgets = {'fk': forms.HiddenInput()}
        fields = ['rech_num', 'dat', 'betrag', 'fk', 'beschr']
        labels = {'rech_num': 'Rechnungsnummer', 'beschr': 'Grund der Ausgaben', 'dat': 'Datum', 'betrag': 'Betrag'}


class MangelForm(ModelForm):
    class Meta:
        model = Mangel
        #widgets = {'fk': forms.HiddenInput()}
        fields = ['text', 'behoben', 'dat', 'fk']
        labels = {'text': 'Beschreibung'}
