from .models import *
from .forms import *

# Funktion, um mittels des Namens eines Models, dynamisch die dazu passende ModelForm zu ermitteln. Wird u.a. in View-Funktionen für generische Templates verwendet, um mit einem Modelnamen eine dazu passende Form zu bestimmen.
def get_modelForm_by_name(model): # Str des Models
    modelForms = {'Fahrt': FahrtForm, 'Fahrer': FahrerForm, 'Fahrzeug': FahrzeugForm, 'Mangel': MangelForm, 'Rechnung': RechnungForm}

    return modelForms[model]

# Funktion, um eine Klasse aus dem bezeichnenden String zu erstellen. Zurückgegeben wird also eine Klasse, auf der Klassenoperationen ausgeführt werden können und keine Instanz. Dies ist z.B. notwendig, um Datenbankabfragen zu bestimmten Models dynamisch zu generieren.
def get_model_by_name(model): # Str des Models
    classes = {'Fahrt': Fahrt, 'Fahrer': Fahrer, 'Fahrzeug': Fahrzeug, 'Mangel': Mangel, 'Rechnung': Rechnung}

    return classes[model]

# Eine Funktion, zur Erstellung einer Liste mit allen vorhandenen Kennzeichen in der Datenbank. Die Listen werden in Templates für den Fuhrparkleiter benötigt, damit er ein passendes Kennzeichen auswählen kann.
def get_signList():
    return [f.kennz for f in Fahrzeug.objects.all()]



# alte Funktionen, nicht in Verwendung
    #

# Funktion, um die Instanz eines Models aus einem String zu erstellen.
# def get_modelInstance(request):
#     modeltype = request.POST['model']
#
#     if modeltype == 'Fahrer':
#         return Fahrer()
#     elif modeltype == 'Fahrt':
#         return Fahrt()
#
#     elif modeltype == 'Fahrzeug':
#         return Fahrzeug()
#
#     elif modeltype == 'Mangel':
#         return Mangel()
#
#     elif modeltype == 'Rechnung':
#         return Rechnung()

    # elif modeltype == 'Fahrt':
    #     if instanciateForm: # muss aus view eintr_aendern kommen
    #         ID = request.POST['model_id'] # id kann nur in request aus list_obj.html kommen
    #         obj = classes[modeltype].objects.get(id=ID)
    #         form = modelForms[modeltype](instance=obj)
    #         #if form.is_valid():
    #         return {'form': form}
    #     if blankForm:
    #         form = FahrtForm()
    #         return {'form': form}
    #
    #     # aus form_hzfg: neue ausgefüllte Form
    #     ID = request.POST['model_id']
    #     obj = classes[modeltype].objects.get(id=ID)
    #     form = FahrtForm(request.POST, instance=obj)
    #     # für eintr_hzfg view, zum abspeichern der in "request" übermittelten Form
    #     return {'form': form}
    #
    #
    # if modeltype == 'Fahrer':
    #     if instanciateForm: # muss aus view eintr_aendern kommen
    #         ID = request.POST['id'] # id kann nur in request aus list_obj.html kommen
    #         obj = Fahrer.objects.get(id=ID)
    #         return {'form': FahrerForm(instance=obj), 'view': 'fahrer_hzfg'}
    #     if blankForm:
    #         form = FahrerForm()
    #         return {'form': form, 'view': 'fahrer_hzfg'}
    #     # für eintr_hzfg view, zum abspeichern der in "request" übermittelten Form
    #     return {'form': FahrerForm(request.POST)}
    #
    #
    # elif modeltype == 'Fahrt':
    #     if instanciateForm: # muss aus view eintr_aendern kommen
    #         ID = request.POST['model_id'] # id kann nur in request aus list_obj.html kommen
    #         obj = classes[modeltype].objects.get(id=ID)
    #         form = FahrtForm(instance=obj)
    #         #if form.is_valid():
    #         return {'form': form}
    #     if blankForm:
    #         form = FahrtForm()
    #         return {'form': form}
    #
    #     # aus form_hzfg: neue ausgefüllte Form
    #     ID = request.POST['model_id']
    #     obj = classes[modeltype].objects.get(id=ID)
    #     form = FahrtForm(request.POST, instance=obj)
    #     # für eintr_hzfg view, zum abspeichern der in "request" übermittelten Form
    #     return {'form': form}
    #
    # elif modeltype == 'Fahrzeug':
    #     if instanciateForm: # muss aus view eintr_aendern kommen
    #         ID = request.POST['model_id'] # id kann nur in request aus list_obj.html kommen
    #         obj = Fahrzeug.objects.get(id=ID)
    #         return {'form': FahrzeugForm(instance=obj), 'view': 'fahrzeug_hzfg'}
    #     if blankForm:
    #         form = FahrzeugForm()
    #         return {'form': form, 'view': 'fahrzeug_hzfg'}
    #     # für eintr_hzfg view, zum abspeichern der in "request" übermittelten Form
    #     return {'form': FahrzeugForm(request.POST), 'view': 'fahrzeug_hzfg'}
    #
    # elif modeltype == 'Mangel':
    #     if instanciateForm: # muss aus view eintr_aendern kommen
    #         ID = request.POST['model_id'] # id kann nur in request aus list_obj.html kommen
    #         obj = Mangel.objects.get(id=ID)
    #         return {'form': MangelForm(instance=obj), 'view': 'mangel_eintr'}
    #     if blankForm:
    #         form = MangelForm()
    #         return {'form': form, 'view': 'mangel_eintr'}
    #     # für eintr_hzfg view, zum abspeichern der in "request" übermittelten Form
    #     return {'form': MangelForm(request.POST), 'view': 'mangel_eintr'}
    #
    #
    # elif modeltype == 'Rechnung':
    #     if instanciateForm: # muss aus view eintr_aendern kommen
    #         ID = request.POST['model_id'] # id kann nur in request aus list_obj.html kommen
    #         obj = Rechnung.objects.get(id=ID)
    #         return {'form': RechnungForm(instance=obj), 'view': 'rech_eintr'}
    #     if blankForm:
    #         form = RechnungForm()
    #         return {'form': form, 'view': 'rech_eintr'}
    #     # für eintr_hzfg view, zum abspeichern der in "request" übermittelten Form
    #     return {'form': RechnungForm(request.POST), 'view': 'rech_eintr'}


    #
    # modeltype = request.POST['model']
    # obj = 0
    #
    # if instanciateForm: # muss aus view eintr_aendern kommen
    #     ID = request.POST['model_id'] # id kann nur in request aus list_obj.html kommen
    #     obj = classes[modeltype].objects.get(id=ID)
    #     form = modelForms[modeltype](instance=obj)
    #     #if form.is_valid():
    #     return {'form': form}
    # if blankForm:
    #     form = modelForms[modeltype]()
    #     return {'form': form}
    #
    # # aus form_hzfg: neue ausgefüllte Form
    # if request.session['usertype'] =='fahrer':
    #     ID = request.POST['model_id']
    #     obj = classes[modeltype].objects.get(id=ID)
    # else:
    #     obj = classes[modeltype]()
    #
    # form = modelForms[modeltype](request.POST, instance=obj)
    # # für eintr_hzfg view, zum abspeichern der in "request" übermittelten Form
    # return {'form': form}
