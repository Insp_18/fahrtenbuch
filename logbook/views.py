# Modul zum Rendern Templates mit weiteren Attributen
from django.shortcuts import render
# Modul um von einer View in eine andere Umzuleiten
from django.shortcuts import redirect
# Module, um eine Http-Ausgaben zu genrieren
from django.http import HttpResponse, HttpResponseNotFound
from django.http import HttpResponseRedirect

# Formen und zusätzliche Funktionen importieren, Models sind automatisch integriert
from .forms import *
from .utils import *
from datetime import date

# Startseite mit Einlog-Funktion rendern. Falls der Nutzer eingeloggt ist, wird er auf seine Startseite umgeleitet.
def index(request):
    if 'login' in request.session and request.session['login'] == True:
        return redirect('user_start')
    else:
        return render(request=request,
                    template_name='logbook/index.html') # immer app/template!

# Logout des Nutzers, seine Sessiondaten werden gelöscht. Zugriff über: header.html
def log_out(request):
    request.session.flush()
    return redirect('index')

# Darstellung einfacher statischer Seiten: Zugriff über: header.html
def support(request):
    return render(request=request, template_name='logbook/support.html')

def impressum(request):
    return render(request=request, template_name='logbook/impressum.html')

def datenschutz(request):
    return render(request=request, template_name='logbook/datenschutz.html')

def kontakt(request):
    return render(request=request, template_name='logbook/kontakt.html')

# In dieser View werden die Login-Angaben des Nutzers überprüft. Ist der Nutzer erfolgreich eingeloggt, wird das Template user_start.html gerendert. Zugriff über: index.html
def user_start(request):
    if request.method == 'POST':
        if request.POST['user'] == '':
            return HttpResponse('Bitte einen Nutzernamen angeben!')

        else:
            name = request.POST['user']
            pw = request.POST['password']

            # Fuhrparkleiter?
            if 'usertype' in request.POST:
                try:
                    user = Fuhrparkleiter.objects.get(name=name)
                    request.session['usertype'] = 'leiter'
                    request.session['kennz'] = '' # Platzhalter
                    request.session['fk'] = 0 # Platzhalter
                except Exception as e:
                    return HttpResponse('Nutzer nicht gefunden!')
            # Fahrer
            else:
                # Fahrer mit Namen vorhanden?
                try:
                    user = Fahrer.objects.get(name=name)
                    request.session['usertype'] = 'fahrer'
                    # request.session['login'] = True
                    request.session['fk'] = user.fk.id
                    request.session['kennz'] = Fahrzeug.objects.get(id=user.fk.id).kennz

                except Exception as e:
                    return HttpResponse('Nutzer nicht gefunden!')

            # Passwort-Check
            if pw == user.password:
                request.session['login'] = True
                request.session['id'] = user.id
                request.session['name'] = user.name

                context = {'user': user, 'usertype': request.session['usertype'], 'kennz': request.session['kennz']}
                return render(request=request,
                                template_name='logbook/user_start.html',
                                context=context)
            # falsches PW
            else:
                return HttpResponse('Falsche(s/r) Nutzername oder Passwort!')

    # für Redirections:
    elif not request.session.is_empty():
        if request.session['login']:
            if request.session['usertype'] == 'leiter':
                user = Fuhrparkleiter.objects.get(id=request.session['id'])
            else:
                user = Fahrer.objects.get(id=request.session['id'])

            context = {'user': user, 'usertype': request.session['usertype'], 'kennz': request.session['kennz']}
            return render(request=request,
                            template_name='logbook/user_start.html',
                            context=context
                            )
    #nicht eingeloggt
    else:
        return HttpResponse('Sie sind nicht eingeloggt!')

# Diese View ermittelt die passenden Account-Daten des Nutzers für das Teplate account.html. Zugriff über: header.html, user_start.html
def account(request):
    if request.session.is_empty():
        return HttpResponse('Sie sind nicht eingeloggt!')

    else:

        user = ''
        msg = ''

        # Fahrer oder Leiter?
        if request.session['usertype'] == 'leiter':
            user = Fuhrparkleiter.objects.get(id=request.session['id'])
        else:
            user = Fahrer.objects.get(id=request.session['id'])

        if request.method == 'POST':
            # Passwortänderung --> von account.html kommend
            if 'pw' in request.POST:
                # stimmen neue Passwörter  überein?
                if request.POST['pw1'] == request.POST['pw2'] and request.POST['pw1'] != '':
                    # Fahrer oder Leiter?
                    # if request.session['usertype'] == 'leiter':
                    #     user = Fuhrparkleiter.objects.get(id=request.session['id'])
                    #
                    # else:
                    #     user = Fahrer.objects.get(id=request.session['id'])

                    # neues PW speichern
                    user.password = request.POST['pw1']
                    user.save()
                    msg = 'Passwort geändert'

                else:
                    return HttpResponse('Eingegebene Passwörter stimmen nicht überein!')


            # Datenänderung (wenn 'pw' nicht in request) --> von account.html kommend
            elif 'email' in request.POST:
                # user.work_id = request.POST['']
                if request.session['usertype'] == 'leiter':
                    if request.POST['name'] != '': user.name = request.POST['name']
                if request.POST['email'] != '': user.email = request.POST['email']
                if request.POST['tel'] != '': user.tel = request.POST['tel']
                user.save()
                msg = 'Daten geändert'

        # Erstaufruf --> von user_start.html kommend
            context = {'user': user, 'usertype': request.session['usertype'], 'msg': msg}
            return render(request=request, template_name='logbook/account.html', context=context)

        elif not request.session.is_empty():
            if request.session['login']:
                if request.session['usertype'] == 'leiter':
                    user = Fuhrparkleiter.objects.get(id=request.session['id'])
                else:
                    user = Fahrer.objects.get(id=request.session['id'])

                context = {'user': user, 'usertype': request.session['usertype'], 'msg': msg}
                return render(request=request, template_name='logbook/account.html', context=context)
        #nicht eingeloggt
        else:
            return HttpResponse('Sie sind nicht eingeloggt!')
#
#####################################################################################
#  generische Funktionen
#

# Diese View ermittelt das passende Model, welches geändert werden soll und rendert dies im Template form.hzfg. Zugriff über: list_obj.html
def eintr_aendern(request):
    if request.method == 'POST':
        modelname = request.POST['model']

        model = get_model_by_name(modelname).objects.get(id=request.POST['model_id'])

        #passende Modelform erstellen
        modelform = get_modelForm_by_name(modelname)(instance=model)

        fahrzList = None
        kennz = None

        if request.session['usertype'] == 'leiter':
            fahrzList = get_signList
            fk = request.POST['model_id']
        else:
            fk = request.session['fk']
            kennz = request.session['kennz']

        context= {'form': modelform, 'transmission': request.POST['model'] + ' ändern', 'model': request.POST['model'], 'view': 'aender_speichern', 'fahrzList': fahrzList, 'kennz': kennz , 'usertype': request.session['usertype'], 'fk': fk, 'model_id': request.POST['model_id']}

        return render(request, 'logbook/form_hzfg.html', context=context)
            # return HttpResponseNotFound('Änderungen nicht mehr möglich!' + '%s' % form.errors + ' whatever')

    else:
        return HttpResponse("Keine POST-Argumente übermittelt!")

# Diese View speichert Änderungen eines Models und rendert das Template form_hzfg.html: von eintr_aendern kommend
def aender_speichern(request):
    if request.method == 'POST':
        modelname = request.POST['model']

        print(request.POST)

        model = get_model_by_name(modelname).objects.get(id=request.POST['model_id'])

        post_copy = request.POST.copy()

        # bei leiter ist fk = 0 in POST, was den fk der Instanz bei modelform.save() überschreiben würde. Daher wird fk neu ermittelt
        if request.session['usertype'] == 'leiter':

            # Bei Änderungen der Fahrerdaten...
            if 'name' in request.POST:
                post_copy['fk'] = Fahrer.objects.get(id=request.POST['fk']).fk
            else:
                post_copy['fk'] = request.POST['model_id']


        modelForm = get_modelForm_by_name(modelname)(post_copy, instance=model)

        if modelForm.is_valid():
            modelForm.save()
        else:
            return HttpResponse('Form nicht zulässig!' + '%s' % modelForm.errors)

        msg = 'Eintragung geändert!'

        modelForm = get_modelForm_by_name(modelname)()

        fahrzList = None
        kennz = None

        if request.session['usertype'] == 'leiter':
            fahrzList = get_signList
        else:
            #fk = request.session['fk']
            kennz = request.session['kennz']

        context= {'form': modelForm, 'model': request.POST['model'], 'transmission': 'Eintrag speichern', 'usertype': request.session['usertype'], 'fahrzList': fahrzList, 'view': 'eintr_speichern', 'kennz': kennz, 'msg': msg}
        return render(request, 'logbook/form_hzfg.html', context=context)

# Diese View speichert neue Einträge/Models in der Datenbank und rendert das Template form_hzfg.html. Von form_hzfg.html kommend (intiales Speichern)
def eintr_speichern(request):
    if request.method == 'POST':
        modelname = request.POST['model']

        post_copy = request.POST.copy()

        # fk in POST nicht enthalten, hier hzfg
        if modelname == 'Mangel' or modelname == 'Rechnung':
            kennz = request.POST['kennz']
            post_copy['fk'] = Fahrzeug.objects.get(kennz=kennz).id

        # aus utils.py modelForm-BEZEICHNUNG erhalten
        modelForm = get_modelForm_by_name(modelname)



        # passende ModelForm mit POST-Daten
        form = modelForm(post_copy)

        # leere Liste mit Kennzeichen
        fahrzList = None

        if form.is_valid():

            # ModelForm mit POST-Daten in DB speichern
            form.save()

            # letzten Kilomoterstand in neuer Form rendern...
            if modelname == 'Fahrt':
                fahrzeug = Fahrzeug.objects.get(kennz=request.session['kennz'])
                km = Fahrt.objects.filter(fk=fahrzeug.id).latest('km_end').km_end
                form = modelForm(initial={'km_start': km, 'km_end': km})

                # ...und in DB speichern
                fahrzeug.km_stand = km
                fahrzeug.save()

            # blanke passende Form erstellen
            else:
                form = modelForm()

            kennz = None

            # Auswahlliste der Kennzeichen der Fahzeuge für Leiter erstellen
            if request.session['usertype'] == 'leiter':
                fahrzList = get_signList()

            # Kennzeichen des Fahrers ermitteln
            if request.session['usertype'] == 'fahrer':
                kennz = request.session['kennz']

            msg = 'Eintragung hinzugefügt'

            # Seite neu laden für weitere Eingaben
            context= {'form': form, 'msg': msg, 'model': modelname, 'transmission': 'Weitere Eintragung vornehmen', 'usertype': request.session['usertype'], 'fahrzList': fahrzList, 'view': 'eintr_speichern', 'kennz': kennz, 'fk': request.session['fk']}

            return render(request, 'logbook/form_hzfg.html', context=context)
            #return render(request, 'logbook/form_hzfg.html', context=context)
        else:
            return HttpResponse('Form nicht zulässig!' + '%s' % form.errors)

# Mit dieser View wird eine Modelinstanz in der Datenbank gelöscht und die entsprechende Übersicht der Models mit dem Template list_obj.html neu gerendert.
def eintr_loeschen(request):
    if request.method == 'POST':
        modelname = request.POST['model']
        model_id = request.POST['model_id']

        model = get_model_by_name(modelname).objects.get(id=model_id)

        model.delete()

        msg = modelname + ' gelöscht!'

        objlist =  get_model_by_name(modelname).objects.all()

        context = {'msg': msg, 'objlist': objlist,'usertype': request.session['usertype']}
        return render(request=request, template_name='logbook/list_obj.html', context=context)

# Diese View ermittelt die passende Form zum Eintrangen einer neuen Modelinstanz und rendert das Template form_hzfg.html. Aufruf aus user_start.html
def eintr_hzfg(request):
    if request.method == 'POST':

        print(request.POST)
        # passende ModelForm ermitteln
        form = get_modelForm_by_name(request.POST['model'])

        # letzten Kilomoterstand in neuer Form für eine neue Fahrt ermitteln
        if request.POST['model'] == 'Fahrt':
            fk = Fahrzeug.objects.get(kennz=request.session['kennz']).id
            if Fahrt.objects.filter(fk=fk).exists():
                km = Fahrt.objects.filter(fk=fk).latest('km_end').km_end
            else:
                km = 0

            # ModelForm "Fahrt" mit letzten Kilometerständen instanziieren
            form = form(initial={'km_start': km, 'km_end': km})

        # blanke passende ModelForm instanziieren
        else:
            form = form()

        fahrzList = None
        kennz = None

        # Falls der Nutzer der Fahrer ist, benötigt er eine Liste der Kennzeichen, um seinen neuen Eintrag einem Fahrzeug zuzuordnen
        if request.session['usertype'] == 'leiter':
            fahrzList = get_signList
        else:
            kennz = request.session['kennz']

        context= {'form': form, 'model': request.POST['model'], 'transmission': 'Eintrag speichern', 'usertype': request.session['usertype'], 'fahrzList': fahrzList, 'view': 'eintr_speichern', 'kennz': kennz, 'fk': request.session['fk']}
        return render(request, 'logbook/form_hzfg.html', context=context)

    else:
        return HttpResponse("Keine POST-Argumente übermittelt!")

# Diese View ermittel alle Fahrer und Fahrzeuge in der DB und rendert diese im Template fahrer_zuweis.html.
def fahrer_zuweis(request):
    if request.method == 'POST':
        # zum Rendern des Templates bei Erstaufruf
        if 'blank' in request.POST:
            fahrerliste = [f.name for f in Fahrer.objects.all()]
            fahrzliste = get_signList()

            context = {'fahrerliste': fahrerliste, 'fahrzliste': fahrzliste}
            return render(request=request, template_name='logbook/fahrer_zuweis.html', context=context)

        # Speichern der Änderungen
        else:
            fahrerN = request.POST['fahrer']
            fahrzN = request.POST['fahrz']

            fahrer = Fahrer.objects.get(name=fahrerN)

            fahrer.fk = Fahrzeug.objects.get(kennz=fahrzN)

            fahrer.save()

            msg = 'Fahrer ' + fahrer.name + ' wurde Fahrzeug ' + fahrer.fk.kennz + ' zugewiesen.'

            fahrerliste = [f.name for f in Fahrer.objects.all()]
            fahrzliste = get_signList()

            context = {'fahrerliste': fahrerliste, 'fahrzliste': fahrzliste, 'msg': msg}
            return render(request=request, template_name='logbook/fahrer_zuweis.html', context=context)

###############################################################################

# Liste von Modelinstanzen, die zu einem bestimmten Fahrzeug gehören
def list_single(request):
    model = get_model_by_name(request.POST['model'])
    # fk hier immer eine fahrzeug-id bei list_single
    fk = Fahrzeug.objects.get(kennz=request.session['kennz']).id
    objlist = model.objects.filter(fk=fk)
    context = {'objlist': objlist}
    return render(request=request, template_name='logbook/list_obj.html', context=context)

# Erstellt Liste aller Objekte eines Models in DB (Fuhrparkleiter)
def list_all(request):
    # ermitteln des Models aus request
    modelname = get_model_by_name(request.POST['model'])
    objlist =  modelname.objects.all()

    # km-Stand updaten
    if request.POST['model'] == 'Fahrzeug':
        for o in objlist:
            if Fahrt.objects.filter(fk=o.id).exists():
                km = Fahrt.objects.filter(fk=o.id).latest('km_end').km_end
                o.km_stand = km
                o.save()

    context = {'objlist': objlist,'usertype': request.session['usertype']}
    return render(request=request, template_name='logbook/list_obj.html', context=context)

###########################################################################
# View zum Anzeigen der Fahrzeugdaten für Fahrer
def fahrzeug_daten(request):
    fahrzeug = Fahrzeug.objects.get(kennz=request.session['kennz'])
    context= {'fahrzeug': fahrzeug, 'listtype': 'eintr_aendern'}
    return render(request=request, template_name='logbook/fahrzeug_daten.html', context=context)




# ##############################################################################
### veraltete nicht-generische Funktionen
# def maengel_single(request):
#     kennz = request.POST['maengel']
#     fk = Fahrzeug.objects.get(fk=request.session['id']).id
#     maengel = Mangel.objects.filter(fk=fk)
#     context = {'maengel': maengel, 'kennz': kennz}
#     return render(request=request, template_name='logbook/maengel_single.html', context=context)
#
# def fahrten_single(request):
#     User_ID = request.session['id']
#     fk = Fahrzeug.objects.get(fk=User_ID).id
#     fahrten = Fahrt.objects.filter(fk=fk)
#     context = {'objlist': fahrten, 'auszulassen': 'fahrz'}
#     return render(request=request, template_name='logbook/list_obj.html', context=context)
#
# def rechnungen_single(request):
#     kennz = request.POST['rechnungen']
#     fk = Fahrzeug.objects.get(fk=request.session['id']).id
#     rechnungen = Rechnung.objects.filter(fk=fk)
#     context = {'objlist': rechnungen, 'auszulassen': ''}
#     return render(request=request, template_name='logbook/list_obj.html', context=context)
#
# def fahrzeuge_list(request):
#     fahrzeuge =  Fahrzeug.objects.all()
#     listtype = request.POST['list']
#     context = {'objlist': fahrzeuge,'auszulassen': '', 'listtype': listtype}
#     return render(request=request, template_name='logbook/list_obj.html', context=context)
#
# def maengel_all(request):
#     maengel = Mangel.objects.all()
#     context = {'objlist': maengel, 'auszulassen': 'Fahrzeug'}
#     return render(request=request, template_name='logbook/list_obj.html', context=context)
#
#
#
#
# def rechnungen_all(request):
#     rechnungen = Rechnung.objects.all()
#     context = {'objlist': rechnungen, 'auszulassen': ''}
#     return render(request=request, template_name='logbook/list_obj.html', context=context)
#
#
#
# def rech_eintr(request):
#     if request.method =='POST':
#          # (nachher)
#         form = ''
#         if request.POST['hzfg'] == 'nachher':
#             form = RechnungForm(request.POST)
#             msg = 'Rechnung eingetragen!'
#             if form.is_valid():
#                 form.save()
#                 context= {'form': form, 'msg': msg, 'formtype': 'rech_eintr', 'transmission': 'weitere Rechnung eintragen'}
#                 return render(request, 'logbook/form_hzfg.html', context=context)
#
#         # (vorher)
#         else:
#             form = RechnungForm()
#             context= {'form': form, 'formtype': 'rech_eintr', 'transmission': 'Rechnung eintragen'}
#             return render(request, 'logbook/form_hzfg.html', context=context)
#
# def mangel_eintr(request):
#     if request.method =='POST':
#          # (nachher)
#         form = ''
#         if request.POST['hzfg'] == 'nachher':
#             form = MangelForm(request.POST)
#             msg = 'Mangel hinzugefügt!'
#             if form.is_valid():
#                 new_fahrer = form.save()
#                 context= {'form': form, 'msg': msg, 'formtype': 'mangel_eintr', 'transmission': 'Weiteren Mangel eintragen'}
#                 return render(request, 'logbook/form_hzfg.html', context=context)
#
#         # (vorher)
#         else:
#             form = MangelForm()
#             context= {'form': form, 'formtype': 'mangel_eintr', 'transmission': 'Mangel eintragen'}
#             return render(request, 'logbook/form_hzfg.html', context=context)
#
#
#
#
# def fahrer_list(request):
#     #listtype = request.POST['list']
#     fahrer = Fahrer.objects.all()
#     context = {'objlist': fahrer, 'auszulassen': 'password', 'listtype': 'fahrer_hzfg'}
#     return render(request=request, template_name='logbook/list_obj.html', context=context)
#
# def fahrer_hzfg(request):
#     # Anzeige von Formular zum hzfg, Fahrer-Model-Daten
#
#     if request.method =='POST':
#         # Fahrer wurde hinzugefügt (nachher), Seiten Reload
#         form = ''
#         if 'hfzg' in request.POST:
#             form = FahrerForm(request.POST)
#             msg = 'Fahrer hinzugefügt!'
#             if form.is_valid():
#                 new_fahrer = form.save()
#                 context= {'form': form, 'msg': msg, 'formtype': 'fahrer_hzfg', 'transmission': 'Fahrer hinzufügen'}
#                 return render(request, 'logbook/form_hzfg.html', context=context)
#             else:
#                 return HttpResponse('<h3>Formular falsch ausgefüllt!</h3>')
#
#         # Fahrer soll geändert werden (vorher)
#         if 'model' in request.POST:
#             form = get_formtype(request)
#             context= {'form': form, 'formtype': 'fahrer_hzfg', 'transmission': 'Fahrerdaten ändern'}
#             return render(request, 'logbook/form_hzfg.html', context=context)
#
#         # Erstaufruf
#         else:
#             form = FahrerForm()
#             context= {'form': form, 'formtype': 'fahrer_hzfg', 'transmission': 'Fahrer hinzufügen'}
#             return render(request, 'logbook/form_hzfg.html', context=context)
#
# def fahrzeug_hzfg(request):
#     # Anzeige von Formular zum hzfg, Fahrer-Model-Daten
#
#     if request.method =='POST':
#         # Fahrer wurde hinzugefügt (nachher) --> von form_hzfg.html kommend
#         form = FahrzeugForm()
#         if 'hzfg' in request.POST:
#             form = FahrzeugForm(request.POST)
#             msg = 'Fahrzeug hinzugefügt!'
#             if form.is_valid():
#                 new_fahrer = form.save()
#                 context= {'form': form, 'msg': msg, 'formtype': 'fahrzeug_hzfg', 'transmission': 'Fahrzeug hinzufügen'}
#                 return render(request, 'logbook/form_hzfg.html', context=context)
#
#         # Änderung --> von list_obj.html kommend
#         if 'model' in request.POST:
#             form = get_formtype(request)
#             context= {'form': form, 'formtype': 'fahrzeug_hzfg', 'transmission': 'Fahrzeugdaten ändern'}
#             return render(request, 'logbook/form_hzfg.html', context=context)
#
#         # Fahrer soll hzgf werden (vorher) --> von user_start.html kommend
#         else:
#             context= {'form': form, 'formtype': 'fahrzeug_hzfg', 'transmission': 'Fahrzeug hinzufügen'}
#             return render(request, 'logbook/form_hzfg.html', context=context)
#
#
# def fahrt_eintr(request):
#     if request.method == 'POST':
#      # (nachher): Eintragung speichern, Seite reloaden
#         form = FahrtForm()
#         if 'hzfg' in request.POST:
#             # DB-form aus request erstellen
#             form = FahrtForm(request.POST)
#             msg = 'Fahrt eingetragen!'
#             if form.is_valid():
#                 # Form-Daten in DB speichern
#                 form.save()
#                 # Seite neu laden für weitere Eingaben
#                 context= {'form': form, 'msg': msg, 'formtype': 'fahrt_eintr', 'transmission': 'Weitere Fahrt eintragen'}
#                 return render(request, 'logbook/form_hzfg.html', context=context)
#
#         # Änderung
#         if 'model' in request.POST:
#             form = get_modelForm(request, True, False)
#             context= {'form': form, 'formtype': 'fahrt_eintr', 'transmission': 'Fahrt eintragen'}
#             return render(request, 'logbook/form_hzfg.html', context=context)
#
#         # (vorher)
#         else:
#             # Form erstellen
#
#             context= {'form': form, 'formtype': 'fahrt_eintr', 'transmission': 'Fahrt eintragen'}
#             return render(request, 'logbook/form_hzfg.html', context=context)
#
#     else:
#         return HttpResponse("Keine POST-Argumente übermittelt!")
#
#
# def fahrzeuge_list(request):
#     fahrzeuge =  Fahrzeug.objects.all()
#     listtype = request.POST['list']
#     context = {'objlist': fahrzeuge,'auszulassen': ''}
#     return render(request=request, template_name='logbook/list_obj.html', context=context)
