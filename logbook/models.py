# Modul für die Model-Komponente des Frameworks
from django.db import models
# Modul, um Regex-Regeln anwenden zu können
from django.core.validators import RegexValidator
# Modul, um Fehler bei der Validierung von Model-Eingaben zu rendern
from django.core.exceptions import ValidationError
# Modul zur Generierung von Text aus Variablen und Strings
from django.utils.translation import gettext_lazy as _

# Module zum Verwenden und Operieren von Zeitformaten
from datetime import timedelta
from django.utils import timezone

# Regex-Regel für die Kennzeicheneingabe
validate_kennz = RegexValidator(r'^[A-Z]{1,3}-[A-Z]{1,2} [1-9][0-9]{0,3}E?$', 'Kennzeichen bitte nach folgendem Schema angeben: ABC-DE 1235E (E ist optional)')

# Regex-Regel für die FIN
validate_fin = RegexValidator(r'^[A-Za-z0-9]{17}', '17-stellige Fahrzeugidentifikationsnummer angeben.')

# Regex-Regel für die Eingabe von Geldbeträgen
validate_money = RegexValidator(r'^[0-9]{1,5}(\.|\,)[0-9]{2}', 'Beträge bis max. 99999.99, zwei Nachkommastellen, ohne €-Zeichen.')

# Regex-Regel für die Eingabe von Adressen
validate_addr = RegexValidator(r'^[A-Za-z]{4,100}(\.| Strasse| Straße| Str\.) [0-9]{1,5}\, [0-9]{5} [A-Za-z]{2,100}', 'Adressen nach dem folgenden Schema angeben: Musterstr.(oder Muster Strasse) 3, 12345 Musterstadt / keine Umlaute (ü=ue), keine Sonderzeichen')

# Validator für Datumsangaben. Verhindert, dass eine Fahrt angelegt oder geändert werden kann, mit einer Datumsangabe von vor 7 Tagen (falls das Datum der Fahrt bewusst falsch angegeben wird (Betrug), kann dies diese Fkt. nicht erkennen). Auch Angaben für die Zukunft werden dadurch unterbunden.
def validate_date(value):
    today = timezone.now()
    if value < today - timedelta(days=7):
        raise ValidationError(
            _('%(value)s ist länger als sieben Tage her!'),
            params={'value': value}
            )
    if value > today:
        raise ValidationError(
            _('%(value)s liegt in der Zukunft!'),
            params={'value': value}
            )

# Datenbankklasse für den User "Fuhrparkleiter"
class Fuhrparkleiter(models.Model):
    name = models.CharField(max_length=128, unique=True)
    leiter_nr = models.CharField(max_length=128, default='Fuhrparkleiter')
    work_id = models.CharField(max_length=32, help_text='Die Mitarbeiternummer in der Ihrer Organisation')
    email = models.EmailField()
    tel = models.CharField(max_length=128)
    password = models.CharField(editable=False, max_length=128,default='12345')

    # Attribut "name" als String ausgeben, wenn das Objekt abgefragt wird. Es stellt eine leichter lesbare Variante der Objektbezeichnung dar
    def __str__(self):
        return self.name

    # Klasse des Objekts zurückgeben (notwendig, da Methode in Templates nicht verfügbar)
    def get_class(self):
        return self.__class__.__name__

# Datenbankklasse für den User "Fahrer"
class Fahrer(models.Model):
    name = models.CharField(verbose_name='Name',max_length=256, unique=True)
    work_id = models.CharField(verbose_name='Mitarbeiternummer',max_length=32, help_text='Die Mitarbeiternummer in Ihrer Organisation')
    fk = models.ForeignKey(verbose_name="Fahrzeug", to='Fahrzeug', on_delete=models.CASCADE, null=True, blank=True) # blank=True, da Fahrer ohne Fahrzeug erzeugt werden kann, Zuweisung erfolgt separat
    email = models.EmailField(verbose_name='E-Mail',max_length=128)
    tel = models.CharField(verbose_name='Tel.',max_length=128)
    password = models.CharField(editable=False, max_length=128, default='12345')

    # Attribut "name" als String ausgeben, wenn das Objekt abgefragt wird. Es stellt eine leichter lesbare Variante der Objektbezeichnung dar
    def __str__(self):
        return self.name

    # diese Funktion gibt die Felder des Models als Python-Queryset zurück
    def get_fields(self):
        return [(field.verbose_name, field.value_to_string(self)) for field in Fahrer._meta.fields]

# Klasse des Objekts zurückgeben (notwendig, da Methode in Templates nicht verfügbar)
    def get_class(self):
        return self.__class__.__name__

    def auszulassen(self):
        return [self.password]

    def get_kennz(self):
        if self.fk:
            return Fahrzeug.objects.get(kennz=self.fk).kennz

# Datenbankklasse für "Fahrten"
class Fahrt(models.Model):
    fk = models.ForeignKey(to='Fahrzeug', verbose_name="Fahrzeug", on_delete=models.CASCADE) # fahrten werden gelöscht, wenn fahzeug gelöscht wird! nicht gesollt
    # Vom Nutzer anegegebenes Datum
    dat = models.DateTimeField(verbose_name='Datum der Fahrt', validators=[validate_date], help_text='Das Datum der Fahrt')
    # Datum der letzten Änderung, automatisch
    dat_aender = models.DateTimeField(verbose_name='letzte Änderung', auto_now=True)
    # Erstellungsdatum, automatisch
    dat_eintr = models.DateTimeField(verbose_name='Datum der Eintragung',auto_now_add=True)
    start = models.CharField(verbose_name='Startadresse',max_length=64, validators=[validate_addr])
    zwi_ziel = models.CharField(verbose_name='Zwischenziel', max_length=64, blank=True, help_text='Keine geforderte Angabe', validators=[validate_addr])
    ziel = models.CharField(verbose_name='Zieladresse',max_length=64, validators=[validate_addr])
    km_start = models.IntegerField(verbose_name='KM-Stand Start',)
    km_end = models.IntegerField(verbose_name='KM-Stand Ziel',)
    # unvoll = models.BooleanField(verbose_name='unvollständig',blank=True, help_text='Markieren Sie hier, falls Sie weiter Angaben zur Fahrt später machen wollen')
    beschr = models.TextField(verbose_name='Anmerkungen',null=True, blank=True) #keine Eintragung erlauben, nicht "0"

    # diese Funktion gibt die Felder des Models als Python-Queryset zurück
    def get_fields(self):
        return [(field.verbose_name, field.value_to_string(self)) for field in Fahrt._meta.fields]

    # Klasse des Objekts zurückgeben (notwendig, da Methode in Templates nicht verfügbar), darin aber tlw. auf Modelnamen zugrgriffen werden muss
    def get_class(self):
        return self.__class__.__name__


    # Vom Framework bereitsgestellte Funktion, die an dieser Stelle überschrieben wird. Mit ihr können die Daten einer bereits vorhanden Model-Instanz untereinadner verglichen und validiert werden. Dies ist mit den o.g. Feld-Validatoren nicht möglich. Hier wird überprüft, ob eine Fahrt, die geändert werden soll, nicht länger als 7 Tage her ist.
    def clean(self):
        if self.id:
            if timezone.now() - timedelta(days=7) > self.dat_eintr:
                raise ValidationError('Sie können den Eintrag nach sieben Tagen nicht mehr ändern.')
            if timezone.now() - timedelta(days=7) > self.dat:
                raise ValidationError('Sie können den Eintrag nach sieben Tagen nicht mehr ändern.')
            if timezone.now() < self.dat:
                raise ValidationError('Keine Eintragungen für die Zukunft möglich.')

    # Funktion, um passendes Kennzeichen des gefahrenen Fahrzeugs zu ermitteln
    def get_kennz(self):
        if self.fk:
            return Fahrzeug.objects.get(kennz=self.fk).kennz

# Datenbankklasse für die "Fahrzeuge"
class Fahrzeug(models.Model):
    kennz = models.CharField(verbose_name='Kennzeichen',primary_key=False, max_length=32, validators=[validate_kennz], unique=True)
    herst = models.CharField(verbose_name='Hersteller', max_length=32)
    fin = models.CharField(verbose_name='FIN',max_length=17, unique=True, validators=[validate_fin])
    zuldat = models.DateTimeField(verbose_name='Zulassung')
    tuev = models.DateTimeField(verbose_name='TÜV-Termin')
    kost_Stelle = models.CharField(verbose_name='Kostenstelle',max_length=64, help_text='Identifikationsnummer Ihrer Buchhaltung')
    km_stand = models.FloatField(verbose_name='KM-Stand',max_length=64, default=0)

    # Attribut "name" als String ausgeben, wenn das Objekt abgefragt wird. Es stellt eine leichter lesbare Variante der Objektbezeichnung dar
    def __str__(self):
        return self.kennz

    # diese Funktion gibt die Felder des Models als Python-Queryset zurück
    def get_fields(self):
        return [(field.verbose_name, field.value_to_string(self)) for field in Fahrzeug._meta.fields]

    # Klasse des Objekts zurückgeben (notwendig, da Methode in Templates nicht verfügbar)
    def get_class(self):
        return self.__class__.__name__  # Methode in Templates nicht verfügbar!

# Datenbankklasse für "Rechnungen"
class Rechnung(models.Model):
    rech_num = models.CharField("Rechnungsnummer", max_length=64)
    dat = models.DateTimeField("Datum", default=timezone.now)
    fk = models.ForeignKey(to='Fahrzeug', verbose_name="Fahrzeug", on_delete=models.CASCADE, null=True, blank=True)
    betrag = models.CharField(max_length=10 ,verbose_name='Betrag in €', default=0, blank=False, null=False, validators=[validate_money])
    beschr = models.CharField(max_length=128, verbose_name='Grund der Kosten', default='')

    # diese Funktion gibt die Felder des Models als Python-Queryset zurück
    def get_fields(self):
        return [(field.verbose_name, field.value_to_string(self)) for field in Rechnung._meta.fields]

    # Klasse des Objekts zurückgeben (notwendig, da Methode in Templates nicht verfügbar)
    def get_class(self):
        return self.__class__.__name__

    # Funktion, um passendes Kennzeichen des gefahrenen Fahrzeugs zu ermitteln
    def get_kennz(self):
        if self.fk:
            return Fahrzeug.objects.get(kennz=self.fk).kennz

# Datenbankklasse für die "Mängel"
class Mangel(models.Model):
    dat = models.DateTimeField("Datum", default=timezone.now)
    fk = models.ForeignKey(to='Fahrzeug', verbose_name="Fahrzeug", on_delete=models.CASCADE, null=True, blank=True)
    text = models.TextField(verbose_name="Beschreibung", max_length=1024, default='')
    behoben = models.CharField(default='NEIN', help_text='Markieren Sie hier, falls der Mangel behoben wurde', max_length=8)


    # diese Funktion gibt die Felder des Models als Python-Queryset zurück
    def get_fields(self):
        return [(field.verbose_name, field.value_to_string(self)) for field in Mangel._meta.fields]

    # Klasse des Objekts zurückgeben (notwendig, da Methode in Templates nicht verfügbar)
    def get_class(self):
        return self.__class__.__name__

    # Funktion, um passendes Kennzeichen des gefahrenen Fahrzeugs zu ermitteln
    def get_kennz(self):
        if self.fk:
            return Fahrzeug.objects.get(kennz=self.fk).kennz
