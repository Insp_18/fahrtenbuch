from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('account', account, name='account'),
    path('aender_speichern', aender_speichern),
    path('datenschutz', datenschutz),
    path('eintr_aendern', eintr_aendern),
    path('eintr_hzfg', eintr_hzfg),
    path('eintr_loeschen', eintr_loeschen),
    path('eintr_speichern', eintr_speichern),
    path('fahrer_zuweis', fahrer_zuweis),
    path('fahrzeug_daten', fahrzeug_daten),
    path('impressum', impressum),
    path('kontakt', kontakt),
    path('list_all', list_all),
    path('list_single', list_single),
    path('log_out', log_out),
    path('support', support),
    path('user_start', user_start, name='user_start'),

]
